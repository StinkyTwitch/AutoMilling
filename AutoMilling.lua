--[[------------------------------------------------------------------------------------------------
	AutoMilling.lua

	License
	This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
	License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

	Credits
	CodeMyLife, Svs, MrTheSoulz
	Based on MrTheSoulz automilling feature in his rotation pack. Made at the request of a user
	in IRC.
--------------------------------------------------------------------------------------------------]]
local autoMillName, autoMill = ...
autoMill.professionToggle = false
autoMill.mortarToggle = false

function autoMill.Cast(spell)
	if FireHack then
		if type(spell) == "number" then
			CastSpellByID(spell)
		else
			CastSpellByName(spell)
		end
	elseif oexecute then
		if type(spell) == "number" then
			oexecute("CastSpellByID(".. spell ..")")
		else
			oexecute("CastSpellByName(\"".. spell .."\")")
		end
	else
		if type(spell) == "number" then
			CastSpellByID(spell, target)
		else
			CastSpellByName(spell, target)
		end
	end
end

function autoMill.CheckBagForMortar()
	for i=0,4 do
		for j=1,GetContainerNumSlots(i) do
			local t={GetItemInfo(GetContainerItemLink(i,j) or 0)}

			if t[1] == "Draenic Mortar" then
				return true
			end
		end
	end
	return false
end

function autoMill.UseItem(item)
	if FireHack then
		UseItemByName(item)
	elseif oexecute then
		if type(item) == "number" then
			oexecute("UseItemByName(".. item ..")")
		else
			oexecute("UseItemByName(\"".. item .."\")")
		end
	else
		UseItemByName(item)
	end

end

function autoMill.Profession(herb)
	if herb == nil then
		herb = "all"
	else
		herb = string.lower(herb)
	end

	if autoMill.professionToggle and IsSpellKnown(51005) and not UnitChannelInfo("player") then
		-- Frostweed
		if (herb == "all" or herb == "frostweed") and GetItemCount(109124,false,false) >= 5 then
			print("Milling Frostweed.")
			autoMill.Cast(51005)
			autoMill.UseItem(109124)
		-- Fireweed
		elseif (herb == "all" or herb == "fireweed") and GetItemCount(109125,false,false) >= 5 then
			print("Milling Fireweed.")
			autoMill.Cast(51005)
			autoMill.UseItem(109125)
		-- Gorgrond Flytrap
		elseif (herb == "all" or herb == "flytrap") and GetItemCount(109126,false,false) >= 5 then
			print("Milling Gorgrond Flytrap.")
			autoMill.Cast(51005)
			autoMill.UseItem(109126)
		-- Starflower
		elseif (herb == "all" or herb == "starflower") and GetItemCount(109127,false,false) >= 5 then
			print("Milling Starflower.")
			autoMill.Cast(51005)
			autoMill.UseItem(109127)
		-- Nagrand Arrowbloom
		elseif (herb == "all" or herb == "arrowbloom") and GetItemCount(109128,false,false) >= 5 then
			print("Milling Nagrand Arrowbloom.")
			autoMill.Cast(51005)
			autoMill.UseItem(109128)
		-- Talador Orchid
		elseif (herb == "all" or herb == "orchid") and GetItemCount(109129,false,false) >= 5 then
			print("Milling Talador Orchid.")
			autoMill.Cast(51005)
			autoMill.UseItem(109129)
		end
	end
end

function autoMill.Mortar(herb)
	if herb == nil then
		herb = "all"
	else
		herb = string.lower(herb)
	end

	local haveMortar = autoMill.CheckBagForMortar()

	if autoMill.mortarToggle and haveMortar and not UnitChannelInfo("player") then
		-- Frostweed
		if (herb == "all" or herb == "frostweed") and GetItemCount(109124,false,false) >= 5 then
			print("Milling Frostweed.")
			autoMill.UseItem(114942)
			autoMill.Cast(114942)
			autoMill.UseItem(109124)
		-- Fireweed
		elseif (herb == "all" or herb == "fireweed") and GetItemCount(109125,false,false) >= 5 then
			print("Milling Fireweed.")
			autoMill.UseItem(114942)
			autoMill.Cast(114942)
			autoMill.UseItem(109125)
		-- Gorgrond Flytrap
		elseif (herb == "all" or herb == "flytrap") and GetItemCount(109126,false,false) >= 5 then
			print("Milling Gorgrond Flytrap.")
			autoMill.UseItem(114942)
			autoMill.Cast(114942)
			autoMill.UseItem(109126)
		-- Starflower
		elseif (herb == "all" or herb == "starflower") and GetItemCount(109127,false,false) >= 5 then
			print("Milling Starflower.")
			autoMill.UseItem(114942)
			autoMill.Cast(114942)
			autoMill.UseItem(109127)
		-- Nagrand Arrowbloom
		elseif (herb == "all" or herb == "arrowbloom") and GetItemCount(109128,false,false) >= 5 then
			print("Milling Nagrand Arrowbloom.")
			autoMill.UseItem(114942)
			autoMill.Cast(114942)
			autoMill.UseItem(109128)
		-- Talador Orchid
		elseif (herb == "all" or herb == "orchid") and GetItemCount(109129,false,false) >= 5 then
			print("Milling Talador Orchid.")
			autoMill.UseItem(114942)
			autoMill.Cast(114942)
			autoMill.UseItem(109129)
		end
	end
end

function autoMill.ProfessionTimer(herb)
	autoMill.professionTimer = C_Timer.NewTicker(2, function() autoMill.Profession(herb) end, nil)
end
function autoMill.MortarTimer(herb)
	autoMill.mortarTimer = C_Timer.NewTicker(2, function() autoMill.Mortar(herb) end, nil)
end

-- Slash Command
SLASH_AUTOMILLCMD1, SLASH_AUTOMILLCMD2 = "/automill", "/am"

-- Slash Command List
function SlashCmdList.AUTOMILLCMD(msg, editbox)
	local command, moretext = msg:match("^(%S*)%s*(.-)$")
	command = string.lower(command)
	moretext = string.lower(moretext)

	if msg == "" then
		if IsSpellKnown(51005) then
			autoMill.professionToggle = not autoMill.professionToggle
			if autoMill.professionToggle then
				print("Auto Inscription milling is on.")
				autoMill.ProfessionTimer('all')
			else
				print("Auto Inscription milling is off.")
				autoMill.professionTimer:Cancel()
			end
			return
		elseif autoMill.CheckBagForMortar() then
			autoMill.mortarToggle = not autoMill.mortarToggle
			if autoMill.mortarToggle then
				print("Draenic Mortar milling is on.")
				autoMill.MortarTimer('all')
			else
				print("Draenic Mortar milling is off.")
				autoMill.mortarTimer:Cancel()
			end
			return
		else
			print("No apparent method to mill herbs was found!")
		end
	elseif command == "all" then
		if IsSpellKnown(51005) then
			autoMill.professionToggle = not autoMill.professionToggle
			if autoMill.professionToggle then
				print("Auto Inscription milling Frostweed is on.")
				autoMill.ProfessionTimer('all')
			else
				print("Auto Inscription milling Frostweed is off.")
				autoMill.professionTimer:Cancel()
			end
			return
		elseif autoMill.CheckBagForMortar() then
			autoMill.mortarToggle = not autoMill.mortarToggle
			if autoMill.mortarToggle then
				print("Draenic Mortar milling Frostweed is on.")
				autoMill.MortarTimer('all')
			else
				print("Draenic Mortar milling Frostweed is off.")
				autoMill.mortarTimer:Cancel()
			end
			return
		else
			print("No apparent method to mill herbs was found!")
		end
	elseif command == "frostweed" then
		if IsSpellKnown(51005) then
			autoMill.professionToggle = not autoMill.professionToggle
			if autoMill.professionToggle then
				print("Auto Inscription milling Frostweed is on.")
				autoMill.ProfessionTimer('frostweed')
			else
				print("Auto Inscription milling Frostweed is off.")
				autoMill.professionTimer:Cancel()
			end
			return
		elseif autoMill.CheckBagForMortar() then
			autoMill.mortarToggle = not autoMill.mortarToggle
			if autoMill.mortarToggle then
				print("Draenic Mortar milling Frostweed is on.")
				autoMill.MortarTimer('frostweed')
			else
				print("Draenic Mortar milling Frostweed is off.")
				autoMill.mortarTimer:Cancel()
			end
			return
		else
			print("No apparent method to mill herbs was found!")
		end
	elseif command == "fireweed" then
		if IsSpellKnown(51005) then
			autoMill.professionToggle = not autoMill.professionToggle
			if autoMill.professionToggle then
				print("Auto Inscription milling Fireweed is on.")
				autoMill.ProfessionTimer('fireweed')
			else
				print("Auto Inscription milling Fireweed is off.")
				autoMill.professionTimer:Cancel()
			end
			return
		elseif autoMill.CheckBagForMortar() then
			autoMill.mortarToggle = not autoMill.mortarToggle
			if autoMill.mortarToggle then
				print("Draenic Mortar milling Fireweed is on.")
				autoMill.MortarTimer('fireweed')
			else
				print("Draenic Mortar milling Fireweed is off.")
				autoMill.mortarTimer:Cancel()
			end
			return
		else
			print("No apparent method to mill herbs was found!")
		end
	elseif command == "flytrap" then
		if IsSpellKnown(51005) then
			autoMill.professionToggle = not autoMill.professionToggle
			if autoMill.professionToggle then
				print("Auto Inscription milling Gorgrond Flytrap is on.")
				autoMill.ProfessionTimer('flytrap')
			else
				print("Auto Inscription milling Gorgrond Flytrap is off.")
				autoMill.professionTimer:Cancel()
			end
			return
		elseif autoMill.CheckBagForMortar() then
			autoMill.mortarToggle = not autoMill.mortarToggle
			if autoMill.mortarToggle then
				print("Draenic Mortar milling Gorgrond Flytrap is on.")
				autoMill.MortarTimer('flytrap')
			else
				print("Draenic Mortar milling Gorgrond Flytrap is off.")
				autoMill.mortarTimer:Cancel()
			end
			return
		else
			print("No apparent method to mill herbs was found!")
		end
	elseif command == "starflower" then
		if IsSpellKnown(51005) then
			autoMill.professionToggle = not autoMill.professionToggle
			if autoMill.professionToggle then
				print("Auto Inscription milling Starflower is on.")
				autoMill.ProfessionTimer('starflower')
			else
				print("Auto Inscription milling Starflower is off.")
				autoMill.professionTimer:Cancel()
			end
			return
		elseif autoMill.CheckBagForMortar() then
			autoMill.mortarToggle = not autoMill.mortarToggle
			if autoMill.mortarToggle then
				print("Draenic Mortar milling Starflower is on.")
				autoMill.MortarTimer('starflower')
			else
				print("Draenic Mortar milling Starflower is off.")
				autoMill.mortarTimer:Cancel()
			end
			return
		else
			print("No apparent method to mill herbs was found!")
		end
	elseif command == "arrowbloom" then
		if IsSpellKnown(51005) then
			autoMill.professionToggle = not autoMill.professionToggle
			if autoMill.professionToggle then
				print("Auto Inscription milling Nagrand Arrowbloom is on.")
				autoMill.ProfessionTimer('arrowbloom')
			else
				print("Auto Inscription milling Nagrand Arrowbloom is off.")
				autoMill.professionTimer:Cancel()
			end
			return
		elseif autoMill.CheckBagForMortar() then
			autoMill.mortarToggle = not autoMill.mortarToggle
			if autoMill.mortarToggle then
				print("Draenic Mortar milling Nagrand Arrowbloom is on.")
				autoMill.MortarTimer('arrowbloom')
			else
				print("Draenic Mortar milling Nagrand Arrowbloom is off.")
				autoMill.mortarTimer:Cancel()
			end
			return
		else
			print("No apparent method to mill herbs was found!")
		end
	elseif command == "orchid" then
		if IsSpellKnown(51005) then
			autoMill.professionToggle = not autoMill.professionToggle
			if autoMill.professionToggle then
				print("Auto Inscription milling Talador Orchid is on.")
				autoMill.ProfessionTimer('orchid')
			else
				print("Auto Inscription milling Talador Orchid is off.")
				autoMill.professionTimer:Cancel()
			end
			return
		elseif autoMill.CheckBagForMortar() then
			autoMill.mortarToggle = not autoMill.mortarToggle
			if autoMill.mortarToggle then
				print("Draenic Mortar milling Talador Orchid is on.")
				autoMill.MortarTimer('orchid')
			else
				print("Draenic Mortar milling Talador Orchid is off.")
				autoMill.mortarTimer:Cancel()
			end
			return
		else
			print("No apparent method to mill herbs was found!")
		end
	elseif command == "help" then
		print("AM: '/automill' or '/am' will turn on/off auto milling.")
		print("AM: '/am frostweed' will mill ONLY Frostweed.")
		print("AM: '/am fireweed' will mill ONLY Fireweed.")
		print("AM: '/am flytrap' will mill ONLY Gorgrond Flytrap.")
		print("AM: '/am starflower' will mill ONLY Starflower.")
		print("AM: '/am arrowbloom' will mill ONLY Nagrand Arrowbloom.")
		print("AM: '/am orchid' will mill ONLY Talador Orchid.")
	end
end